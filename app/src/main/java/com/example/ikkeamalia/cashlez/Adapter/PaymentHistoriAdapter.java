package com.example.ikkeamalia.cashlez.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cashlez.android.sdk.payment.CLPaymentResponse;
import com.example.ikkeamalia.cashlez.R;

import java.util.List;

public class PaymentHistoriAdapter extends RecyclerView.Adapter<PaymentHistoriAdapter.PaymentViewHolder> {

    Context mContext;
    List<CLPaymentResponse> paymentResponseList;

    public PaymentHistoriAdapter (Context context, List<CLPaymentResponse> paymentResponseList){
        this.mContext = context;
        this.paymentResponseList = paymentResponseList;
    }


    @NonNull
    @Override
    public PaymentHistoriAdapter.PaymentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_histori, viewGroup, false);
        return new PaymentViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentHistoriAdapter.PaymentViewHolder holder, int position) {
        final CLPaymentResponse paymentResponse = paymentResponseList.get(position);
        holder.amount.setText(paymentResponse.getAmount());
        if (paymentResponse.getTransactionType()!=null) {
            holder.trxType.setText(paymentResponse.getTransactionType().getValue());
        }
        holder.trxStatus.setText(String.valueOf(paymentResponse.getTransactionStatus()));
        holder.trxTime.setText(paymentResponse.getTransTime());
        holder.trxDate.setText(paymentResponse.getTransDate());
        holder.traceNo.setText(paymentResponse.getTraceNo());
        holder.cardNo.setText(paymentResponse.getCardNo());
        holder.approvalCode.setText(paymentResponse.getApprovalCode());


    }

    @Override
    public int getItemCount() {
        return paymentResponseList.size();
    }

    public class PaymentViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView amount, trxType, trxStatus, trxTime, trxDate, traceNo, cardNo, approvalCode;
        public PaymentViewHolder(@NonNull View itemView) {
            super(itemView);

            amount = itemView.findViewById(R.id.history_row_amount);
            trxType = itemView.findViewById(R.id.history_row_transaction_type);
            trxStatus = itemView.findViewById(R.id.history_row_transaction_status);
            trxTime = itemView.findViewById(R.id.history_row_transaction_time);
            trxDate = itemView.findViewById(R.id.history_row_transaction_date);
            traceNo = itemView.findViewById(R.id.history_row_trace_number);
            cardNo = itemView.findViewById(R.id.history_row_card_number);
            approvalCode = itemView.findViewById(R.id.history_row_approval_code);



        }
    }
}
