package com.example.ikkeamalia.cashlez.Utilities;

import com.cashlez.android.sdk.CLMerchant;

public class UserManager {

    private static UserManager instance;

    public static UserManager getInstance(){
        if( instance == null ){
            instance = new UserManager();
        }
        return instance;
    }

    public CLMerchant clMerchant = new CLMerchant();

    public CLMerchant getClMerchant() {
        return clMerchant;
    }

    public void setClMerchant(CLMerchant clMerchant) {
        this.clMerchant = clMerchant;
    }
}
