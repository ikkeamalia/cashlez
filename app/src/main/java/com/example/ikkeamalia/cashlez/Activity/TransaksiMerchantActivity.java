package com.example.ikkeamalia.cashlez.Activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.cashlez.android.sdk.CLErrorResponse;
import com.cashlez.android.sdk.login.CLLoginHandler;
import com.cashlez.android.sdk.payment.CLPaymentResponse;
import com.cashlez.android.sdk.paymenthistory.CLPaymentHistoryHandler;
import com.cashlez.android.sdk.paymenthistory.CLPaymentHistoryResponse;
import com.cashlez.android.sdk.paymenthistory.ICLPaymentHistoryHandler;
import com.cashlez.android.sdk.paymenthistory.ICLPaymentHistoryService;
import com.example.ikkeamalia.cashlez.Adapter.PaymentHistoriAdapter;
import com.example.ikkeamalia.cashlez.R;

import java.util.ArrayList;
import java.util.List;

public class TransaksiMerchantActivity extends RootActivity implements ICLPaymentHistoryService {

    RecyclerView rvTrans;
    List<CLPaymentResponse> paymentResponseList;
    PaymentHistoriAdapter adapter;
    LinearLayoutManager manager;

    ICLPaymentHistoryService iclPaymentHistoryService;
    ICLPaymentHistoryHandler historyHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_transactions);

        rvTrans = findViewById(R.id.rvTransactions);
        paymentResponseList = new ArrayList<>();
        manager = new LinearLayoutManager(TransaksiMerchantActivity.this);

        historyHandler = new CLPaymentHistoryHandler(TransaksiMerchantActivity.this, this);
        showLoading();
        historyHandler.doGetSalesHistory(1, "", "");

    }


    @Override
    public void onSalesHistorySuccess(CLPaymentHistoryResponse clPaymentHistoryResponse) {
        if (clPaymentHistoryResponse.isSuccess()){
            hideLoading();
            adapter = new PaymentHistoriAdapter(TransaksiMerchantActivity.this, clPaymentHistoryResponse.getPaymentList());
            rvTrans.setLayoutManager(manager);
            rvTrans.setAdapter(adapter);
        }
    }

    @Override
    public void onSalesHistoryError(CLErrorResponse clErrorResponse) {
        hideLoading();
        Toast.makeText(TransaksiMerchantActivity.this, clErrorResponse.getErrorMessage(), Toast.LENGTH_SHORT).show();

    }
}
