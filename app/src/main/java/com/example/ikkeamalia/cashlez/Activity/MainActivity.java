package com.example.ikkeamalia.cashlez.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cashlez.android.sdk.CLMerchant;
import com.example.ikkeamalia.cashlez.R;
import com.example.ikkeamalia.cashlez.Utilities.UserManager;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {

    CLMerchant clMerchant;
    Button btnDataMerchant, btnTrans;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnDataMerchant = findViewById(R.id.btnDataMerchant);
        btnTrans = findViewById(R.id.btnSemuaTransaksi);

        btnDataMerchant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog merchant = new Dialog(MainActivity.this);
                merchant.setContentView(R.layout.dialog_data_merchant);

                TextView nama = merchant.findViewById(R.id.namaMerchant);
                TextView alamat1 = merchant.findViewById(R.id.alamat1);
                TextView alamat2 = merchant.findViewById(R.id.alamat2);
                TextView kota = merchant.findViewById(R.id.kota);
                Button ok = merchant.findViewById(R.id.ok);
                ImageView logo = merchant.findViewById(R.id.logoMerchant);

                if (UserManager.getInstance().getClMerchant()!=null){
                    nama.setText(UserManager.getInstance().getClMerchant().getName());
                    Picasso.with(MainActivity.this).load(UserManager.getInstance().getClMerchant().getLogoUrl())
                    .into(logo);
                    alamat1.setText(UserManager.getInstance().getClMerchant().getAddressFirst() );
                    alamat2.setText(UserManager.getInstance().getClMerchant().getAddressSecond());
                    kota.setText(UserManager.getInstance().getClMerchant().getCity());


                }

                merchant.show();

                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        merchant.dismiss();
                    }
                });
            }
        });

        btnTrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, TransaksiMerchantActivity.class);
                startActivity(i);
            }
        });

    }
}
