package com.example.ikkeamalia.cashlez.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.Toast;

import com.cashlez.android.sdk.CLErrorResponse;
import com.cashlez.android.sdk.login.CLLoginHandler;
import com.cashlez.android.sdk.login.CLLoginResponse;
import com.cashlez.android.sdk.login.ICLLoginService;
import com.example.ikkeamalia.cashlez.R;
import com.example.ikkeamalia.cashlez.Utilities.UserManager;

public class LoginActivity extends RootActivity implements ICLLoginService {

    TextInputEditText username, pin;
    AppCompatButton loginButton;

    private final String serverPublicKey = "<RSAPublicKey>\n" +
            "\t<Modulus>wdrcLwl0A26rIr/HE7GQ2cpPZX49ezl781nDs5y+4QR6BdcMxuWuaUInbI3yBcitWX4O/ii21TPtXNpOTsNU7fmHK/Jna9cCnqgJPNANLkZK5mHz2feB/eYxJX9cCTY00e/Ot7L8mhsZepct0MUVZxwWu/ky3eHhAS7RyVwAJzehol+gx6T4rZechWQidAbSTkJMoCjPG1/bhoP+et+uTKE4Nxe1DLybW3wWii30RObh1EalbVfduVZJ/1wRyqbah+r1XuTRq7/l2DEUkdtaoGszouK4kC8zS0CivvF0DPT+0CcBLRLjUE2wdCtjZEhITwfcVhMfSNKpseGlF3N8qw==</Modulus>\n" +
            "\t<Exponent>AQAB</Exponent>\n" +
            "</RSAPublicKey>";

    private final String clientPrivateKey = "<RSAKeyValue>\n" +
            "\t<Modulus>xRLQqutpZhj84YIbGrvX91sJlWITxA51ADnpcEq6KVZGzSwMjmGx1hDzhGfNGbYRXPMbR+WL7XZVLC5Rt8ctShY/IOIRvkSrUL5siQo5TlSf1jm0DrAgPogvbxouAS48OSFzDAhAX2ZEu0fOo2EKswrAvoxWRPKLoswo19bYTV7QImwUqM3Cz4inCMnyeRAD2G51SDSX0v+bZXAma9Nfw2uLDJQylTVKJSOaHevYhOg8XsLQszgY01wLaERrVP0oUDJwSMP8gkZ70MwC8NfAwH+EdnIV16idvEpMJx2KSl0RKa7Ga0JooxwG92rxvjI1ZKvuyqq2tTn/S16pvmfDLQ==</Modulus>\n" +
            "\t<Exponent>AQAB</Exponent>\n" +
            "\t<D>RNfzySF/qkjkXirDKS2hnilRSbv/R6f9O1z/rViNe89F4HqY0ExUgYFecEyKm4a8vgm26ADKRuPlkQ+FQHv12EG5P9V5eUwnxIchByZpKnHpfD6gJdllCjMsztUIaSUrqJGEzRk805a4P+wvxWcrA6yn9Gi14lQQb4h+ZBgLa8ssW63EBHMLrGFIQdE205ut1wz99+U5dfKAXf6LRWINxf72I41fwXapvXBtO6nvfYo1V8WQfq6HmCBD1Xy9hLGLgI/lGnFixZD3KtbfRG5YdRky+aBdHqoBvR4IDnlevubz2G8qKODY2qnTUsSlwL4XTw8QawKKxJpjq02ol2zsiQ==</D>\n" +
            "\t<P>3UhusOKgQBWRZ83dVOPh0fCoTWGVXUzCyDth/eUj/VD4yrsV+GPfXh32t03mWZJIkQC63+qe/Y9OOMLEtLySrLUnDZUV3JSe6JyImuEhd+azWkKDOMmUeHqZu53UDljRdkExcCfBqRIfF2ePFl803+7AqDdgfC+zFjQagtz8uB8=</P>\n" +
            "\t<Q>4/4KAaOGLqqz8mAZW80bZJ5dxZ+xTk2oQfjYLhK8UgJOp4LPng+J/n/fJDQndCyYKui41hnEiMCZr/Z450QAp4yxjajC166izaSvtr7ZL0uFoM/coOgowrVhhG6GGWHzKgWYnUTJ7RUbSKc6vcSc8da6dgfigdPPaU6I94YiSzM=</Q>\n" +
            "\t<DP>GDL2n/N4sOlq6F40CTeOl4Xo8eVtDzH4zyrnUXvAjtBPFOSWx34sjD9cnrkvKrZ7pxfcV+ZxkqscU8rA9j71D1wUNEEMjf3WzvtnWQCrx0/8Zy+E6C3rRa2qqEDfUt5Vscf5XxmJ7TJlIgsaM8kfoCmc+ghsTchtnkz+ZTdDj+M=</DP>\n" +
            "\t<DQ>F5/wW4EdW2KW6OuqVQfo6cE7SEom7k0/vS5TAFsypnDUw6jbaK6Fhxiq/65j1Db2waOB27Wp1t3WTxSELLqwMqxyjZJKNl1DQ3noN1CJYsw5mZNQcl/8MUjoRPfK74Bl2RnhWZKSNf306M9jV9yywqCUi7x/bSKMoIXhzXxZu18=</DQ>\n" +
            "\t<InverseQ>uvd2bogXKkeWGxdHRyV2fpHNL+kpATuZx6ZTLMoYeCN9DRqKmB67DPCkBXZKAsck3J75L9fM0MT+TeR6P+/vu9OEQ8tV769q7oA15Yk4OleQgmbG8auaXQ4T2E8K9LKxTOHQXzYFtvRpGGZS8LvgvV8tQUhO6Jg/KEInXBQlA/8=</InverseQ>\n" +
            "</RSAKeyValue>";

    private CLLoginHandler loginHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        username = findViewById(R.id.login_user_name);
        pin = findViewById(R.id.login_pin);
        loginButton = findViewById(R.id.login_btn);

        loginHandler = new CLLoginHandler(LoginActivity.this, this);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!username.getText().toString().equalsIgnoreCase("") && !pin.getText().toString().equalsIgnoreCase("") ){
                    showLoading();
                    loginHandler.doLogin(username.getText().toString(), pin.getText().toString());
                } else {
                    Toast.makeText(LoginActivity.this, "Harap lengkapi data", Toast.LENGTH_SHORT).show();
                }
            }
        });





    }

    @Override
    public void onStartActivation(String s) {

    }

    @Override
    public void onLoginSuccess(CLLoginResponse clLoginResponse) {
        if(clLoginResponse.isSuccess()) {
            UserManager.getInstance().setClMerchant(clLoginResponse.getMerchant());
            hideLoading();
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(i);
        }
    }

    @Override
    public void onLoginError(CLErrorResponse clErrorResponse) {
        hideLoading();


        Toast.makeText(LoginActivity.this, clErrorResponse.getErrorMessage(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onNewVersionAvailable(CLErrorResponse clErrorResponse) {

    }

    @Override
    public void onApplicationExpired(CLErrorResponse clErrorResponse) {

    }

//    @NonNull
//    @Override
//    public LoginPresenter createPresenter() {
//        ApplicationState applicationState = (ApplicationState) getApplicationContext();
//        return new LoginPresenter(this, applicationState);
//    }
}
