package com.example.ikkeamalia.cashlez.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.example.ikkeamalia.cashlez.R;

public class RootActivity extends AppCompatActivity {

   // ProgressBar progressBar;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);

        pd = new ProgressDialog(this);
        pd.setMessage("Loading");
        pd.setCancelable(false);


    }

    public void showLoading(){
        if (!pd.isShowing()){
            pd.show();
        }
    }

    public void hideLoading(){
        if (pd.isShowing()){
            pd.dismiss();
        }
    }






}
